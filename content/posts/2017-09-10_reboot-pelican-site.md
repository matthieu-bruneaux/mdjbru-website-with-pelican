Title: Website reboot using Pelican
Date: 2017-09-10
Tags: Internet, Python

Today, I started to re-organize my website using a pure Pelican solution.

The aim was to start the website (again!) from a clean slate and to use
[Gitlab.com](https://gitlab.com/) both to host the website repository and to
publish the webpages.

Some bookmarks:

- [A Pelican tutorial](https://www.fullstackpython.com/blog/generating-static-websites-pelican-jinja2-markdown.html)
- [Markdown basics](https://daringfireball.net/projects/markdown/basics)
